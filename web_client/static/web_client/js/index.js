var get_data_list = [];
var host = '127.0.0.1:8000';

function overcome_x_frame_options($iframe, post_pk) {
    var iframe = $iframe[0];
    var url = iframe.src;

    get_data_list[post_pk] = function(data) {
        if (data && data.query && data.query.results && data.query.results.resources && data.query.results.resources.content && data.query.results.resources.status == 200) loadHTML(data.query.results.resources.content);
        else if (data && data.error && data.error.description) loadHTML(data.error.description);
        else loadHTML('Error: Cannot load ' + url);
    };

    var loadURL = function (src) {
        var script = document.createElement('script');
        script.src = 'http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20data.headers%20where%20url%3D%22' + encodeURIComponent(url) + '%22&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=get_data_list[' + post_pk + ']';
        document.body.appendChild(script);
    };
    var loadHTML = function (html) {
        iframe.src = 'about:blank';
        timer = setTimeout(function() {
            iframe.contentWindow.document.open();
            iframe.contentWindow.document.write(html.replace(/<head>/i, '<head><base href="' + url + '"><scr' + 'ipt>document.addEventListener("click", function(e) { if(e.target && e.target.nodeName == "A") { e.preventDefault(); parent.loadURL(e.target.href); } });</scr' + 'ipt>'));
            iframe.contentWindow.document.close();
            iframe.height = iframe.contentWindow.document.body.scrollHeight;
        }, 100);
    }
    loadURL(iframe.src);
}

var Destinations = {
    list: [],
    render: function() {
        var $destinations_container = $('.posting-destinations');
        var destination_template = _.template($('#reply-destination').text());
        $destinations_container.empty();
        Destinations.list.forEach(function(destination_pk, index, destinations) {
            var $destination = $(destination_template({destination_pk: destination_pk}));
            $destination.find('.posting-destinations__remove').click(function() {
                Destinations.list.splice(Destinations.list.indexOf(destination_pk), 1);
                Destinations.render();
            });
            $destinations_container.append($destination);
        });
    }
}

var Page = {
    THREAD_LIST: 'thread_list',
    THREAD_DETAILS: 'thread_details',
    active: null
}

var ThreadDetails = {
    activate: function(thread_pk) {
        var thread_details_url = 'http://' + host + '/thread-details/';
        var data = {
            thread_pk: thread_pk
        };
        Page.thread_pk = thread_pk;
        $.get(thread_details_url, data, function(response) {
            ThreadDetails.render(response.thread_data);
            Page.active = Page.THREAD_DETAILS;
        });
    },

    render: function(thread_data) {
        var $content = $('.content');
        var post_template = _.template($('#post').text());
        $content.empty();
        var post_list = thread_data.post_list;
        var $return_to_thread_list = $('<div class="return-to-thread-list">К списку тредов</div>');
        $return_to_thread_list.click(function() {
            ThreadList.activate();
        });
        $content.append($return_to_thread_list);
        post_list.forEach(function(post, index, post_list) {
            var context = {
                'post': post
            };
            var $post = $(post_template(context));
            $post.find('.post__reply').click(function() {
                if (Destinations.list.indexOf(post.pk) != -1) {
                    return;
                }
                Destinations.list.push(post.pk);
                Destinations.render();
            });
            $content.append($post);
            overcome_x_frame_options($post.find('.post__content'), post.pk);
        });
    }
}

var ThreadList = {
    activate: function() {
        var thread_list_url = 'http://' + host + '/thread-list/';
        $.get(thread_list_url, {}, function(response) {
            ThreadList.render(response.thread_list);
            Page.active = Page.THREAD_LIST;
        });
    },

    render: function(thread_list) {
        var $content = $('.content');
        $content.empty();
        var thread_template = _.template($('#thread').text());
        thread_list.forEach(function(thread, index, thread_list) {
            var context = {
                'thread': thread
            };
            var $thread = $(thread_template(context));
            $content.append($thread);
            $thread.find('.thread__reply').click(function() {
                ThreadDetails.activate(thread.pk);
            });
            overcome_x_frame_options($thread.find('.thread__op'), thread.op.pk);
        });
    }
}


$(function() {
    var $content = $('.content');
    var $telegraph_slug = $('.posting__telegraph-slug');
    var $posting = $('.posting');
    var $posting_send_button = $posting.find('.posting__send');

    function create_thread() {
        var create_thread_url = 'http://' + host + '/create-thread/';
        var data = {
            telegraph_slug: $telegraph_slug.val()
        };
        $.get(create_thread_url, data, function(response) {
            ThreadList.activate();
        });
    }

    function create_post() {
        var create_post_url = 'http://' + host + '/create-post/';
        var data = {
            telegraph_slug: $telegraph_slug.val(),
            thread_pk: Page.thread_pk,
            destinations: Destinations.list
        };
        $.get(create_post_url, data, function(response) {
            ThreadDetails.activate(Page.thread_pk);
        });
    }

    function init_posting_panel() {
        $posting_send_button.click(function() {
            if (Page.active == Page.THREAD_LIST) {
                create_thread();
            } else if (Page.active == Page.THREAD_DETAILS) {
                create_post();
            }
        });
    }

    init_posting_panel();
    ThreadList.activate();
});