"""graph_chan URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from main.views import *
from web_client.views import *
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    # SERVER
    url(r'^admin/', admin.site.urls),
    url(r'^create-thread/$', create_thread),
    url(r'^create-post/$', create_post),
    url(r'^thread-list/$', thread_list),
    url(r'^thread-details/$', thread_details),

    # CLIENT
    url(r'^$', Index.as_view()),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
