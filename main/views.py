from main.models import *
from util import json_response


@json_response
def create_thread(request):
    thread = Thread.objects.create()
    post = Post.objects.create(thread=thread, telegraph_slug=request.GET.get('telegraph_slug'))
    return {
        'ok': True,
        'thread_pk': thread.pk,
        'post_pk': post.pk,
    }


@json_response
def create_post(request):
    thread = Thread.objects.get(pk=request.GET.get('thread_pk'))
    destinations = request.GET.getlist('destinations[]')
    post = Post.objects.create(thread=thread, telegraph_slug=request.GET.get('telegraph_slug'))
    post.destinations.add(*destinations)
    return {
        'ok': True,
        'post_pk': post.pk,
    }


@json_response
def thread_list(request):
    return {
        'ok': True,
        'thread_list': map(Thread.web_data, Thread.objects.all()),
    }


@json_response
def thread_details(request):
    thread = Thread.objects.get(pk=request.GET.get('thread_pk'))
    thread_web_data = thread.web_data()
    thread_web_data['post_list'] = map(Post.web_data, Post.objects.filter(thread=thread))
    return {
        'ok': True,
        'thread_data': thread_web_data,
    }
