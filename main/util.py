import json
from django.http import HttpResponse


def json_response(view):
    def wrap(*args, **kwargs):
        data = view(*args, **kwargs)
        return HttpResponse(json.dumps(data), content_type="application/json")
    return wrap
