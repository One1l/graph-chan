# -*- coding: utf-8 -*-
from django.db import models


class Thread(models.Model):
    def get_op(self):
        return Post.objects.filter(thread=self).first()

    def web_data(self):
        return {
            'pk': self.pk,
            'op': self.get_op().web_data(),
        }


class Post(models.Model):
    telegraph_slug = models.CharField(max_length=256, verbose_name=u'Slug')
    thread = models.ForeignKey(Thread, verbose_name=u'Тред')
    destinations = models.ManyToManyField('Post', verbose_name=u'Адресаты')

    def web_data(self):
        return {
            'pk': self.pk,
            'thread_pk': self.thread.pk,
            'telegraph_slug': self.telegraph_slug,
            'destinations': map(lambda _: _.pk, self.destinations.all()),
        }
